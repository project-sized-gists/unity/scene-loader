﻿namespace Conduct.Core
{
    /// <summary>
    /// An interface for initializable and deinitializable objects.
    /// </summary>
    public interface IInitializable
    {
        #region Methods

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Deinitializes this instance.
        /// </summary>
        void Deinitialize();

        #endregion Methods
    }
}