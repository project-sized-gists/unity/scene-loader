﻿using System.Collections;

namespace Conduct.Core
{
    /// <inheritdoc />
    /// <summary>
    /// An interface for initializable and deinitializable <see cref="T:UnityEngine.MonoBehaviour"/> objects.
    /// </summary>
    /// <seealso cref="T:Conduct.Core.IInitializable" />
    public interface IMonoInitializable : IInitializable
    {
        #region Methods

        /// <summary>
        /// Initializes this instance using a coroutine.
        /// </summary>
        /// <returns></returns>
        IEnumerator InitializeCoroutine();

        /// <summary>
        /// Deinitializes this instance using a coroutine.
        /// </summary>
        /// <returns></returns>
        IEnumerator DeinitializeCoroutine();

        #endregion Methods
    }
}