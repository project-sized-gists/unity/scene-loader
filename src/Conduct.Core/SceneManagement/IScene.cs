﻿namespace Conduct.Core.SceneManagement
{
    /// <inheritdoc />
    /// <summary>
    /// A <see cref="IMonoInitializable"/> data representation of a <see cref="T:UnityEngine.SceneManagement.Scene"/>.
    /// </summary>
    /// <seealso cref="T:Conduct.Core.IMonoInitializable" />
    public interface IScene : IMonoInitializable
    {
        #region Properties

        /// <summary>
        /// Gets a value indicating whether this instance is loaded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is loaded; otherwise, <c>false</c>.
        /// </value>
        bool IsLoaded { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid { get; }

        /// <summary>
        /// Gets the scene objects of this instance.
        /// </summary>
        /// <value>
        /// The scene objects.
        /// </value>
        ISceneObject[] SceneObjects { get; }

        #endregion Properties
    }
}