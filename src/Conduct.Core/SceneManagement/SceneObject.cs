﻿using System.Collections;
using UnityEngine;

namespace Conduct.Core.SceneManagement
{
    /// <inheritdoc cref="ISceneObject" />
    /// <summary>
    /// An abstract implementation of <see cref="ISceneObject"/>.
    /// </summary>
    /// <seealso cref="T:UnityEngine.MonoBehaviour" />
    /// <seealso cref="T:Conduct.Core.SceneManagement.ISceneObject" />
    public abstract class SceneObject : MonoBehaviour, ISceneObject
    {
        #region Methods

        /// <inheritdoc />
        public virtual void Initialize() => StartCoroutine(InitializeCoroutine());

        /// <inheritdoc />
        public virtual void Deinitialize() => StartCoroutine(DeinitializeCoroutine());

        /// <inheritdoc />
        public abstract IEnumerator InitializeCoroutine();

        /// <inheritdoc />
        public abstract IEnumerator DeinitializeCoroutine();

        #endregion Methods
    }
}