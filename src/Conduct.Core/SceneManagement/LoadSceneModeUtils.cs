﻿using System;

namespace Conduct.Core.SceneManagement
{
    using ULoadSceneMode = UnityEngine.SceneManagement.LoadSceneMode;

    /// <summary>
    /// A utility class for <see cref="LoadSceneMode"/>.
    /// </summary>
    public static class LoadSceneModeUtils
    {
        #region Static Methods

        /// <summary>
        /// Converts to <see cref="LoadSceneMode"/> to <see cref="T:UnityEngine.SceneManagement.LoadSceneMode"/>.
        /// </summary>
        /// <param name="loadSceneMode">The load scene mode.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException">loadSceneMode - null</exception>
        public static ULoadSceneMode ConvertToUnityLoadSceneMode(LoadSceneMode loadSceneMode)
        {
            switch (loadSceneMode)
            {
                case LoadSceneMode.Single:
                    return ULoadSceneMode.Single;

                case LoadSceneMode.Additive:
                    return ULoadSceneMode.Additive;

                default:
                    throw new ArgumentOutOfRangeException(nameof(loadSceneMode), loadSceneMode, null);
            }
        }

        /// <summary>
        /// Converts to <see cref="LoadSceneMode"/> to <see cref="T:UnityEngine.SceneManagement.LoadSceneMode"/>.
        /// </summary>
        /// <param name="loadSceneMode">The load scene mode.</param>
        /// <returns></returns>
        public static ULoadSceneMode ToUnityLoadSceneMode(this LoadSceneMode loadSceneMode)
        {
            return ConvertToUnityLoadSceneMode(loadSceneMode);
        }

        #endregion Static Methods
    }
}