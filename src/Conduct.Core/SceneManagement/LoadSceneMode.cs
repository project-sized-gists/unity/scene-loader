﻿namespace Conduct.Core.SceneManagement
{
    /// <summary>
    /// Used when loading a scene in a player.
    /// </summary>
    public enum LoadSceneMode
    {
        /// <summary>
        /// Closes all current loaded scenes and loads a scene.
        /// </summary>
        Single,

        /// <summary>
        /// Adds the scene to the current loaded scenes.
        /// </summary>
        Additive
    }
}