﻿using System;
using System.Collections;
using System.Linq;

namespace Conduct.Core.SceneManagement
{
    using UScene = UnityEngine.SceneManagement.Scene;
    using USceneManager = UnityEngine.SceneManagement.SceneManager;

    /// <inheritdoc cref="IScene" />
    /// <summary>
    /// A simple implementation of <see cref="IScene"/>.
    /// </summary>
    /// <seealso cref="T:Conduct.Core.SceneManagement.IScene" />
    public struct Scene : IScene
    {
        #region Ctor

        private Scene(string name)
        {
            var uScene = USceneManager.GetSceneByName(name);

            IsValid = uScene.IsValid();

            if (!IsValid)
                throw new Exception($"Invalid scene '{name}'.");

            IsLoaded = uScene.isLoaded;
            SceneObjects = GetSceneObjects(uScene);
        }

        #endregion Ctor

        #region Properties

        /// <inheritdoc />
        public bool IsLoaded { get; }

        /// <inheritdoc />
        public bool IsValid { get; }

        /// <inheritdoc />
        public ISceneObject[] SceneObjects { get; }

        #endregion Properties

        #region Methods

        /// <inheritdoc />
        public void Initialize() => InitializeInternal(this);

        /// <inheritdoc />
        public void Deinitialize() => DeinitializeInternal(this);

        /// <inheritdoc />
        public IEnumerator InitializeCoroutine() => InitializeCoroutineInternal(this);

        /// <inheritdoc />
        public IEnumerator DeinitializeCoroutine() => DeinitializeCoroutineInternal(this);

        #endregion Methods

        #region Static Methods

        /// <summary>
        /// Gets a <see cref="Scene"/> of [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Scene Get(string name)
        {
            return new Scene(name);
        }

        /// <summary>
        /// Gets the scene objects from a <see cref="T:UnityEngine.SceneManagement.Scene"/>.
        /// </summary>
        /// <param name="uScene">The unity scene.</param>
        /// <returns></returns>
        public static ISceneObject[] GetSceneObjects(UScene uScene)
        {
            return uScene
                .GetRootGameObjects()
                .Select(rootGameObject => rootGameObject.GetComponent<ISceneObject>())
                .Where(sceneObject => sceneObject != null)
                .ToArray();
        }

        /// <summary>
        /// Initializes this <see cref="Scene"/>.
        /// </summary>
        /// <param name="scene">The scene.</param>
        public static void InitializeInternal(Scene scene)
        {
            if (!scene.IsValid ||
                !scene.IsLoaded)
                return;

            foreach (var sceneObject in scene.SceneObjects)
                sceneObject.Initialize();
        }

        /// <summary>
        /// Deinitializes this <see cref="Scene"/>.
        /// </summary>
        /// <param name="scene">The scene.</param>
        public static void DeinitializeInternal(Scene scene)
        {
            if (!scene.IsValid ||
                !scene.IsLoaded)
                return;

            foreach (var sceneObject in scene.SceneObjects)
                sceneObject.Deinitialize();
        }

        /// <summary>
        /// Initializes this <see cref="Scene"/> using a coroutine.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public static IEnumerator InitializeCoroutineInternal(Scene scene)
        {
            if (!scene.IsValid ||
                !scene.IsLoaded)
                yield break;

            foreach (var sceneObject in scene.SceneObjects)
                yield return sceneObject.InitializeCoroutine();
        }

        /// <summary>
        /// Deinitializes this <see cref="Scene"/> using a coroutine.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public static IEnumerator DeinitializeCoroutineInternal(Scene scene)
        {
            if (!scene.IsValid ||
                !scene.IsLoaded)
                yield break;

            foreach (var sceneObject in scene.SceneObjects)
                yield return sceneObject.DeinitializeCoroutine();
        }

        #endregion Static Methods
    }
}