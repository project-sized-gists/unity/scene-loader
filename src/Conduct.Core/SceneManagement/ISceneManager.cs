﻿using System.Collections;

namespace Conduct.Core.SceneManagement
{
    /// <summary>
    /// An interface that loads and unloads <see cref="T:UnityEngine.SceneManagement.Scene"/>.
    /// </summary>
    public interface ISceneManager
    {
        #region Methods

        /// <summary>
        /// Loads a scene.
        /// </summary>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="loadSceneMode">The load scene mode.</param>
        void LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single);

        /// <summary>
        /// Unloads a scene.
        /// </summary>
        /// <param name="sceneName">Name of the scene.</param>
        void UnloadScene(string sceneName);

        /// <summary>
        /// Loads a scene using a coroutine.
        /// </summary>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="loadSceneMode">The load scene mode.</param>
        /// <param name="initialize">if set to <c>true</c> [initialize] the scene.</param>
        /// <returns></returns>
        IEnumerator LoadSceneCoroutine(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single, bool initialize = false);

        /// <summary>
        /// Unloads a scene using a coroutine.
        /// </summary>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="deinitialize">if set to <c>true</c> [deinitialize] the scene.</param>
        /// <returns></returns>
        IEnumerator UnloadSceneCoroutine(string sceneName, bool deinitialize = false);

        #endregion Methods
    }
}