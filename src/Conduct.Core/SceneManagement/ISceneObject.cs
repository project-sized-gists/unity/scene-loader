﻿namespace Conduct.Core.SceneManagement
{
    /// <inheritdoc />
    /// <summary>
    /// A <see cref="IMonoInitializable"/> data representation of a <see cref="T:UnityEngine.GameObject"/> inside a <see cref="T:UnityEngine.SceneManagement.Scene"/>.
    /// </summary>
    /// <seealso cref="T:Conduct.Core.IMonoInitializable" />
    public interface ISceneObject : IMonoInitializable
    {
    }
}