﻿using System;
using System.Collections;
using UnityEngine;

namespace Conduct.Core.SceneManagement
{
    using USceneManager = UnityEngine.SceneManagement.SceneManager;

    /// <inheritdoc />
    /// <summary>
    /// A simple implementation of <see cref="ISceneManager"/>.
    /// </summary>
    /// <seealso cref="T:Conduct.Core.SceneManagement.ISceneManager" />
    public sealed class SceneManager : ISceneManager
    {
        #region Ctor

        private SceneManager()
        {
        }

        #endregion Ctor

        #region Methods

        /// <inheritdoc />
        public void LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            USceneManager.LoadScene(sceneName, loadSceneMode.ToUnityLoadSceneMode());
        }

        /// <inheritdoc />
        [Obsolete]
        public void UnloadScene(string sceneName)
        {
            USceneManager.UnloadScene(sceneName);
        }

        /// <inheritdoc />
        public IEnumerator LoadSceneCoroutine(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single, bool initialize = false)
        {
            var asyncOperation = USceneManager.LoadSceneAsync(sceneName, loadSceneMode.ToUnityLoadSceneMode());

            while (!asyncOperation.isDone)
                yield return null;

            yield return new WaitForEndOfFrame();

            if (initialize)
                yield return Scene.Get(sceneName).InitializeCoroutine();
        }

        /// <inheritdoc />
        public IEnumerator UnloadSceneCoroutine(string sceneName, bool deinitialize = false)
        {
            if (deinitialize)
                yield return Scene.Get(sceneName).DeinitializeCoroutine();

            var asyncOperation = USceneManager.UnloadSceneAsync(sceneName);

            while (!asyncOperation.isDone)
                yield return null;

            yield return new WaitForEndOfFrame();
        }

        #endregion Methods

        #region Static Methods

        /// <summary>
        /// Creates a <see cref="SceneManager"/>.
        /// </summary>
        /// <returns></returns>
        public static SceneManager Create() =>
            new SceneManager();

        #endregion Static Methods
    }
}