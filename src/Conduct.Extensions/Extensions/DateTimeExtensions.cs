﻿using System;

namespace Conduct.Extensions
{
    /// <summary>
    /// A collection of extension methods for <see cref="DateTime"/>.
    /// </summary>
    public static class DateTimeExtensions
    {
        #region Static Methods

        public static int Age(this DateTime dateOfBirth)
        {

        }

        /// <summary>
        /// Determines whether [the specified value] [is valid <see cref="DateTime"/>].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is valid <see cref="DateTime"/>]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidDateTime(this string value)
        {
            return value.IsNotNullOrEmpty() &&
                   DateTime.TryParse(value, out _);
        }

        #endregion Static Methods
    }
}