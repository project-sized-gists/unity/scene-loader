﻿namespace Conduct.Extensions
{
    /// <summary>
    /// A collection of extension methods for <see cref="object"/>.
    /// </summary>
    public static class ObjectExtensions
    {
        #region Static Methods

        /// <summary>
        /// Determines whether [the specified value] [is not null].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is not null]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNull(this object value)
        {
            return value != null;
        }

        /// <summary>
        /// Determines whether [the specified value] [is null].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is null]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNull(this object value)
        {
            return value == null;
        }

        #endregion Static Methods
    }
}