﻿using System.Net.Mail;

namespace Conduct.Extensions
{
    /// <summary>
    /// A collection of extension methods for <see cref="string"/>.
    /// </summary>
    public static class StringExtensions
    {
        #region Static Methods

        /// <summary>
        /// Determines whether [the specified value] [is not null or empty].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is not null or empty]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Determines whether [the specified value] [is null or empty].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is null or empty]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Determines whether [the specified value] [is valid email].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is valid email]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidEmail(this string value)
        {
            try
            {
                if (value.IsNullOrEmpty())
                    return false;

                var mailAddress = new MailAddress(value);
                return mailAddress.Address == value;
            }
            catch
            {
                return false;
            }
        }

        #endregion Static Methods
    }
}