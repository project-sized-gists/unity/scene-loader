﻿using System;

namespace Conduct.Extensions
{
    /// <summary>
    /// A collection of extension methods for <see cref="Uri"/>.
    /// </summary>
    public static class UriExtensions
    {
        #region Static Methods

        /// <summary>
        /// Determines whether [the specified value] [is valid <see cref="Uri"/>] and matches [the specified scheme].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="scheme">The scheme.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is valid <see cref="Uri"/>] and matches [the specified scheme]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUri(this string value, string scheme)
        {
            return value.IsNotNullOrEmpty() &&
                   Uri.TryCreate(value, UriKind.Absolute, out var uri) &&
                   uri.Scheme == scheme;
        }

        /// <summary>
        /// Determines whether [the specified value] [is valid <see cref="Uri"/>] and matches [the specified <see cref="UriKind"/>].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="uriKind">Kind of the URI.</param>
        /// <returns>
        ///   <c>true</c> if [the specified value] [is valid <see cref="Uri"/>] and matches [the specified <see cref="UriKind"/>]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUri(this string value, UriKind uriKind)
        {
            return value.IsNotNullOrEmpty() &&
                   Uri.TryCreate(value, uriKind, out _);
        }

        #endregion Static Methods
    }
}