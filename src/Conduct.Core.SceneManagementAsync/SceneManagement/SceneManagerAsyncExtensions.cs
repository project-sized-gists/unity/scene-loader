﻿using System.Threading.Tasks;

namespace Conduct.Core.SceneManagement
{
    /// <summary>
    /// A collection of async scene management methods.
    /// </summary>
    public static class SceneManagerAsyncExtensions
    {
        #region Static Methods

        /// <summary>
        /// Loads a scene asynchronously.
        /// </summary>
        /// <param name="sceneManager">The scene manager.</param>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="loadSceneMode">The load scene mode.</param>
        /// <returns></returns>
        public static async Task LoadSceneAsync(this ISceneManager sceneManager, string sceneName, LoadSceneMode loadSceneMode)
        {
            await sceneManager.LoadSceneCoroutine(sceneName, loadSceneMode);
        }

        /// <summary>
        /// Loads a scene asynchronously.
        /// </summary>
        /// <param name="sceneManager">The scene manager.</param>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="loadSceneMode">The load scene mode.</param>
        /// <param name="initializeScene">if set to <c>true</c> [initialize scene].</param>
        /// <returns></returns>
        public static async Task LoadSceneAsync(this ISceneManager sceneManager, string sceneName, LoadSceneMode loadSceneMode, bool initializeScene)
        {
            await sceneManager.LoadSceneCoroutine(sceneName, loadSceneMode, initializeScene);
        }

        /// <summary>
        /// Unloads a scene asynchronously.
        /// </summary>
        /// <param name="sceneManager">The scene manager.</param>
        /// <param name="sceneName">Name of the scene.</param>
        /// <returns></returns>
        public static async Task UnloadSceneAsync(this ISceneManager sceneManager, string sceneName)
        {
            await sceneManager.UnloadSceneCoroutine(sceneName);
        }

        /// <summary>
        /// Unloads a scene asynchronously.
        /// </summary>
        /// <param name="sceneManager">The scene manager.</param>
        /// <param name="sceneName">Name of the scene.</param>
        /// <param name="deinitializeScene">if set to <c>true</c> [deinitialize scene].</param>
        /// <returns></returns>
        public static async Task UnloadSceneAsync(this ISceneManager sceneManager, string sceneName, bool deinitializeScene)
        {
            await sceneManager.UnloadSceneCoroutine(sceneName, deinitializeScene);
        }

        #endregion Static Methods
    }
}