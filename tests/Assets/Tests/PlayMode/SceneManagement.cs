﻿using Conduct.Core.SceneManagement;
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace Conduct.Tests
{
    using USceneManager = UnityEngine.SceneManagement.SceneManager;

    internal class SceneManagement
    {
        [UnityTest]
        public IEnumerator IsSceneLoaded()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Assert
            Assert.IsTrue(Scene.Get(TEST_SCENE_NAME).IsLoaded);
        }

        [UnityTest]
        public IEnumerator LoadScene_Additive()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            sceneManager.LoadScene(SAMPLE_SCENE_NAME, LoadSceneMode.Additive);
            yield return new WaitForSecondsRealtime(1);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadScene_Single()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            sceneManager.LoadScene(SAMPLE_SCENE_NAME);
            yield return new WaitForSecondsRealtime(1);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneCoroutine_Additive()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneCoroutine_Additive_InitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive, true);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneCoroutine_Single()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneCoroutine_Single_InitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Single, true);

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator UnloadSceneCoroutine()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act 1
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive);

            // Assert 1
            var uScene1 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene1.isLoaded);

            // Act 2
            yield return sceneManager.UnloadSceneCoroutine(SAMPLE_SCENE_NAME);

            // Assert 2
            var uScene2 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsFalse(uScene2.isLoaded);
        }

        [UnityTest]
        public IEnumerator UnloadSceneCoroutine_DeinitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act 1
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive, true);

            // Assert 1
            var uScene1 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene1.isLoaded);

            // Act 2
            yield return sceneManager.UnloadSceneCoroutine(SAMPLE_SCENE_NAME, true);

            // Assert 2
            var uScene2 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsFalse(uScene2.isLoaded);
        }
    }
}