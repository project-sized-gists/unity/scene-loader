﻿using Conduct.Core.SceneManagement;
using NUnit.Framework;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.TestTools;

namespace Conduct.Tests
{
    using USceneManager = UnityEngine.SceneManagement.SceneManager;

    internal class SceneManagementAsync
    {
        internal class MonoSceneManagementAsyncTest : MonoBehaviour, IMonoBehaviourTest
        {
            public bool IsTestFinished { get; private set; }

            public async void Run(Task task)
            {
                await task;

                IsTestFinished = true;

                Destroy(gameObject, 1);
            }
        }

        [UnityTest]
        public IEnumerator LoadSceneAsync_Additive()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.LoadSceneAsync(SAMPLE_SCENE_NAME, LoadSceneMode.Additive));
            yield return mono;

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneAsync_Additive_InitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.LoadSceneAsync(SAMPLE_SCENE_NAME, LoadSceneMode.Additive, true));
            yield return mono;

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneAsync_Single()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.LoadSceneAsync(SAMPLE_SCENE_NAME, LoadSceneMode.Single));
            yield return mono;

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator LoadSceneAsync_Single_InitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.LoadSceneAsync(SAMPLE_SCENE_NAME, LoadSceneMode.Single, true));
            yield return mono;

            // Assert
            var uScene = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene.isLoaded);
        }

        [UnityTest]
        public IEnumerator UnloadSceneAsync()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act 1
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive);

            // Assert 1
            var uScene1 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene1.isLoaded);

            // Act 2
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.UnloadSceneAsync(SAMPLE_SCENE_NAME));
            yield return mono;

            // Assert 2
            var uScene2 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsFalse(uScene2.isLoaded);
        }

        [UnityTest]
        public IEnumerator UnloadSceneAsync_DeinitializeScene()
        {
            // Arrange
            const string TEST_SCENE_NAME = "TestScene";
            const string SAMPLE_SCENE_NAME = "SampleScene";
            var sceneManager = SceneManager.Create();
            yield return USceneManager.LoadSceneAsync(TEST_SCENE_NAME);

            // Act 1
            yield return sceneManager.LoadSceneCoroutine(SAMPLE_SCENE_NAME, LoadSceneMode.Additive, true);

            // Assert 1
            var uScene1 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsTrue(uScene1.isLoaded);

            // Act 2
            var mono = new MonoBehaviourTest<MonoSceneManagementAsyncTest>();
            mono.component.Run(sceneManager.UnloadSceneAsync(SAMPLE_SCENE_NAME, true));
            yield return mono;

            // Assert 2
            var uScene2 = USceneManager.GetSceneByName(SAMPLE_SCENE_NAME);
            Assert.IsFalse(uScene2.isLoaded);
        }
    }
}