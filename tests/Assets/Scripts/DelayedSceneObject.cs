﻿using Conduct.Core.SceneManagement;
using System.Collections;
using UnityEngine;

public class DelayedSceneObject : SceneObject
{
    public float InitializeDelay = 0.5f;

    public float DeinitializeDelay = 1.0f;

    public override IEnumerator InitializeCoroutine()
    {
        yield return new WaitForSecondsRealtime(InitializeDelay);
    }

    public override IEnumerator DeinitializeCoroutine()
    {
        yield return new WaitForSecondsRealtime(DeinitializeDelay);
    }
}